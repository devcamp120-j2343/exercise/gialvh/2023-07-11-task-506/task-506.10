//khai báo thư viện express
const express = require("express");

//khai báo app
const app = express();

//khai báo cổng
const port = 8000;

const methodMiddleware = (req, res,next)=>{
    console.log(req.method);
}

//viết gộp midleware
app.use((req,res,next)=>{
    console.log(new Date());

    next();
}, methodMiddleware)

/*
app.use((req,res,next) =>{
    console.log(req.method);
    next();
})
*/

app.post(`/`,(req,res,next)=>{
    console.log(req.method);
    next();
})

app.get('/', (req,res)=>{
    let today = new Date();
    
    res.status(200).json({
        messsage :`hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.post('/', (req,res)=>{
    let today = new Date();
    
    res.status(200).json({
         messsage : `post method`
    })
})

//khởi động app
app.listen(port, () => {
    console.log("app listening on port : ", port);
})